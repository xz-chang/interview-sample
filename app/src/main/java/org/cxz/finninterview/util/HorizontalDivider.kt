package org.cxz.finninterview.util

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class HorizontalDivider(val space:Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State?) {
        val position = parent.getChildAdapterPosition(view)

        outRect.top = space
        outRect.bottom = space
        if (position == 0) {
            outRect.left = space * 2
        } else {
            outRect.left = space
        }
        outRect.right = space
    }
}