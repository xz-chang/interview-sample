package org.cxz.finninterview.util

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import com.squareup.picasso.Picasso
import org.cxz.finninterview.R
import org.cxz.finninterview.data.Advertisement
import org.cxz.finninterview.data.Image
import java.util.*

const val IMAGE_BASE_URL = "https://images.finncdn.no/dynamic/480x360c/"

object BindingAdaptersUtil {
    @BindingAdapter("app:items")
    @JvmStatic fun setItems(recyclerView: RecyclerView, items: List<Advertisement>) {
        with (recyclerView.adapter as ReplaceableAdapter) {
            replaceData(items)
        }
    }

    @BindingAdapter("app:randomItems")
    @JvmStatic fun setRandomItems(recyclerView: RecyclerView, items: List<Advertisement>) {
        with (recyclerView.adapter as ReplaceableAdapter) {
            if (items.isNotEmpty()) {
                val start = Random().nextInt(items.size)
                val end = Random().nextInt(items.size)
                replaceData(items.subList(Math.min(start, end), Math.max(start, end)))
            } else {
                replaceData(items)
            }

        }
    }

    @BindingAdapter("app:imageUrl")
    @JvmStatic fun loadImage(view: ImageView, image: Image?) {
        Picasso.get().load(IMAGE_BASE_URL + image?.url)
                .placeholder(R.drawable.baseline_photo_black_36)
                .into(view)
    }
}