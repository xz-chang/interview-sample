package org.cxz.finninterview.util

import org.cxz.finninterview.data.Advertisement

interface ReplaceableAdapter {
    fun replaceData(newAds: List<Advertisement>)
}