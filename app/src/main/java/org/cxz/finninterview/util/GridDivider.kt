package org.cxz.finninterview.util

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class GridDivider(val space:Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State?) {
        val position = parent.getChildAdapterPosition(view)

        val itemCount = state!!.itemCount

        if (position % 2 == 0) {
            outRect.left = space * 2
            outRect.right = space
        } else {
            outRect.left = space
            outRect.right = space * 2
        }
        outRect.top = if (position == 0 || position == 1) 0 else space
        if (position == itemCount - 1 || position == itemCount - 2 && itemCount % 2 == 0) {
            outRect.bottom = space * 2
        } else {
            outRect.bottom = space
        }
    }
}