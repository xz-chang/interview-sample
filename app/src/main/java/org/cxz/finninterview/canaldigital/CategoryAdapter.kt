package org.cxz.finninterview.canaldigital

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import org.cxz.finninterview.R
import org.cxz.finninterview.data.Advertisement
import org.cxz.finninterview.databinding.ItemviewCategoryBinding
import org.cxz.finninterview.util.ReplaceableAdapter

class CategoryAdapter(): RecyclerView.Adapter<CategoryItemViewHolder>(), ReplaceableAdapter {
    private var advertisements: List<Advertisement> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryItemViewHolder {
        val binding = DataBindingUtil.inflate<ItemviewCategoryBinding>(
                LayoutInflater.from(parent.context),
                R.layout.itemview_category,
                parent,
                false
        )

        return CategoryItemViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return advertisements.size

    }

    override fun replaceData(newAds: List<Advertisement>) {
        this.advertisements = newAds
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: CategoryItemViewHolder, position: Int) {
        with (holder.binding) {
            advertisement = advertisements[position]
            executePendingBindings()
        }
    }

}