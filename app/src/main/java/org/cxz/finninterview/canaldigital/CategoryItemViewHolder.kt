package org.cxz.finninterview.canaldigital

import android.support.v7.widget.RecyclerView
import org.cxz.finninterview.databinding.ItemviewCategoryBinding

class CategoryItemViewHolder(val binding: ItemviewCategoryBinding): RecyclerView.ViewHolder(binding.root) {
}