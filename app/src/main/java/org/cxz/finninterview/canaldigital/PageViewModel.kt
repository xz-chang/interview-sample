package org.cxz.finninterview.canaldigital

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.cxz.finninterview.data.Advertisement
import org.cxz.finninterview.data.source.AdRepository

class PageViewModel(val app: Application): AndroidViewModel(app) {
    val repo = AdRepository(app)
    val items = ObservableArrayList<Advertisement>()


    fun loadAds() {
        repo.getLocalAds().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object: DisposableObserver<List<Advertisement>>(){
                    override fun onComplete() {
                    }

                    override fun onNext(t: List<Advertisement>) {
                        onAdsLoaded(t)
                    }

                    override fun onError(e: Throwable) {
                    }

                })
    }

    private fun onAdsLoaded(ads: List<Advertisement>) {
        items.clear()
        items.addAll(ads)
    }


}