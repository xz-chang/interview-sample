package org.cxz.finninterview.canaldigital

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_page.*
import org.cxz.finninterview.R
import org.cxz.finninterview.databinding.FragmentPageBinding
import org.cxz.finninterview.util.HorizontalDivider


private const val ARG_PARAM1 = "param1"

class PageFragment : Fragment() {
    private var param1: Int = 0
    private lateinit var viewModel: PageViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getInt(ARG_PARAM1)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val viewDataBinding = FragmentPageBinding.inflate(inflater, container, false).apply {
            viewModel = (activity as MainActivity).obtainViewModel()
        }
        viewModel = viewDataBinding.viewModel!!

        viewDataBinding.setLifecycleOwner(this)

        return  viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        page_indicator.text = "Page: " + arguments!!.getInt(ARG_PARAM1)

        category1.adapter = CategoryAdapter()
        category1.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category2.adapter = CategoryAdapter()
        category2.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category3.adapter = CategoryAdapter()
        category3.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category4.adapter = CategoryAdapter()
        category4.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category5.adapter = CategoryAdapter()
        category5.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category6.adapter = CategoryAdapter()
        category6.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category7.adapter = CategoryAdapter()
        category7.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category8.adapter = CategoryAdapter()
        category8.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category9.adapter = CategoryAdapter()
        category9.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category10.adapter = CategoryAdapter()
        category10.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category11.adapter = CategoryAdapter()
        category11.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category12.adapter = CategoryAdapter()
        category12.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category13.adapter = CategoryAdapter()
        category13.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category14.adapter = CategoryAdapter()
        category14.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category15.adapter = CategoryAdapter()
        category15.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category16.adapter = CategoryAdapter()
        category16.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category17.adapter = CategoryAdapter()
        category17.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category18.adapter = CategoryAdapter()
        category18.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category19.adapter = CategoryAdapter()
        category19.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

        category20.adapter = CategoryAdapter()
        category20.addItemDecoration(HorizontalDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.loadAds()
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: Int) =
                PageFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_PARAM1, param1)
                    }
                }
    }
}
