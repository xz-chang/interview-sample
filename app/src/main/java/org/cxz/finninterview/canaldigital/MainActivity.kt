package org.cxz.finninterview.canaldigital

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import org.cxz.finninterview.R

class MainActivity : AppCompatActivity() {
    private fun onPageChange(newIndex: Int) {
        when(newIndex) {
            0 -> setTitle(getString(R.string.page_title_home))
            1 -> setTitle(getString(R.string.page_title_film_series))
            2 -> setTitle(getString(R.string.page_title_tv))
            3 -> setTitle(getString(R.string.page_title_catch_up))
        }

    }

    lateinit var bottomNavigationView: BottomNavigationView;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_classfied_ad_list)
        bottomNavigationView = findViewById(R.id.bottom_navigation)
        bottomNavigationView.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.action_home -> switchToFragment(0)
                R.id.action_film_series -> switchToFragment(1)
                R.id.action_tv -> switchToFragment(2)
                R.id.action_catch_up -> switchToFragment(3)
            }

            true
        }

        supportFragmentManager.beginTransaction().add(R.id.fragment_container, PageFragment.newInstance(0)).commit()
        onPageChange(0)
    }

    private fun switchToFragment(i: Int) {
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, PageFragment.newInstance(i)).commit()
        onPageChange(i)
    }

    fun obtainViewModel(): PageViewModel = ViewModelProviders.of(this).get(PageViewModel::class.java)

}
