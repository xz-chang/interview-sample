package org.cxz.finninterview.finn

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import org.cxz.finninterview.R
import org.cxz.finninterview.data.Advertisement
import org.cxz.finninterview.databinding.ItemviewAdBinding
import org.cxz.finninterview.util.ReplaceableAdapter

class AdListAdapter(private val viewModel2: AdListViewModel): RecyclerView.Adapter<AdListItemViewHolder>(), ReplaceableAdapter {
    private var advertisements: List<Advertisement> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdListItemViewHolder {
        val binding = DataBindingUtil.inflate<ItemviewAdBinding>(
                LayoutInflater.from(parent.context),
                R.layout.itemview_ad,
                parent,
                false
        )

        return AdListItemViewHolder(binding)
    }

    override fun replaceData(newAds: List<Advertisement>) {
        this.advertisements = newAds
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return advertisements.size
    }

    override fun onBindViewHolder(holder: AdListItemViewHolder, position: Int) {
        with (holder.binding) {
            if (advertisements[position]?.isFavorite == true) {
                holder.favoriteImageView.setImageResource(R.drawable.baseline_favorite_black_36)
            } else {
                holder.favoriteImageView.setImageResource(R.drawable.ic_heart_outline_white_36dp)
            }
            advertisement = advertisements[position]
            viewModel = viewModel2
            executePendingBindings()
        }
    }

}