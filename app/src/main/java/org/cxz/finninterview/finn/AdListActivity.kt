package org.cxz.finninterview.finn

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.cxz.finninterview.R

class AdListActivity : AppCompatActivity() {
    private lateinit var viewModel: AdListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adlist)

        viewModel = obtainViewModel().apply {

        }
    }

    fun obtainViewModel(): AdListViewModel = ViewModelProviders.of(this).get(AdListViewModel::class.java)

}
