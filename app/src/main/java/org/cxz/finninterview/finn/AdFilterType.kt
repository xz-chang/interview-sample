package org.cxz.finninterview.finn

enum class AdFilterType {
    ALL_FILTER,
    FAVORITE_FILTER
}