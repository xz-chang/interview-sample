package org.cxz.finninterview.finn

import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import org.cxz.finninterview.R
import org.cxz.finninterview.databinding.ItemviewAdBinding

class AdListItemViewHolder(val binding: ItemviewAdBinding): RecyclerView.ViewHolder(binding.root) {
    val favoriteImageView:ImageView = binding.root.findViewById(R.id.image_ad_grid_item_favorite)
}