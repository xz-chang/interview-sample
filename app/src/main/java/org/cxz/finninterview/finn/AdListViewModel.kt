package org.cxz.finninterview.finn

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.cxz.finninterview.R
import org.cxz.finninterview.data.Advertisement
import org.cxz.finninterview.data.source.AdRepository
import org.cxz.finninterview.data.source.AdsDataSource

class AdListViewModel(val app: Application): AndroidViewModel(app) {
    val items = ObservableArrayList<Advertisement>()

    val currentFilterLabel = ObservableField<String>()

    val loading = ObservableField<Boolean>()

    val isEmpty = ObservableField<Boolean>()

    val emptyReason = ObservableField<String>()

    val repo = AdRepository(app)

    private var currentFiltering = AdFilterType.ALL_FILTER

    fun flipFavorite(which: Advertisement) {
        val flipped = !(which.isFavorite == true)
        repo.setFavorite(which.id, flipped, object: AdsDataSource.FavoriteSavedCallback{
            override fun onAdFavSaved() {
                val forceUpdate = ArrayList<Advertisement>()
                forceUpdate.addAll(items)
                forceUpdate.forEach {
                    if (it.id == which.id) {
                        it.isFavorite = flipped
                    }
                }

                items.clear()
                items.addAll(forceUpdate)

            }
        })
    }


    fun loadAds() {
        loading.set(true)
        emptyReason.set(app.getString(R.string.adlist_no_ads))
        repo.getAds().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object: DisposableObserver<List<Advertisement>>(){
                    override fun onComplete() {
                    }

                    override fun onNext(t: List<Advertisement>) {
                        onAdsLoaded(t)
                    }

                    override fun onError(e: Throwable) {
                        if (items.isEmpty()) {
                            loading.set(false)
                            isEmpty.set(true)
                            emptyReason.set(app.getString(R.string.adlist_internal_error))
                        }
                    }

                })

    }

    private fun onAdsLoaded(ads: List<Advertisement>) {
        val tasksToShow = ArrayList<Advertisement>()

        if (currentFiltering == AdFilterType.ALL_FILTER) {
            tasksToShow.addAll(ads)
        } else {
            tasksToShow.addAll(ads.filter { it.isFavorite == true })
        }
        setFiltering(currentFiltering)

        items.clear()
        items.addAll(tasksToShow)
        isEmpty.set(tasksToShow.isEmpty())
        loading.set(false)
    }

    fun setFiltering(filterType: AdFilterType) {
        currentFiltering = filterType

        if (filterType == AdFilterType.ALL_FILTER) {
            currentFilterLabel.set(app.getString(R.string.adlist_filter_indicator_all))
        } else {
            currentFilterLabel.set(app.getString(R.string.adlist_filter_indicator_fav))
        }
    }
}