package org.cxz.finninterview.finn

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.PopupMenu
import android.view.*
import kotlinx.android.synthetic.main.fragment_adlist.*
import org.cxz.finninterview.R
import org.cxz.finninterview.canaldigital.MainActivity
import org.cxz.finninterview.databinding.FragmentAdlistBinding
import org.cxz.finninterview.util.GridDivider

class AdListFragment : Fragment() {
    private lateinit var viewModel: AdListViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val viewDataBinding = FragmentAdlistBinding.inflate(inflater, container, false).apply {
            viewModel = (activity as AdListActivity).obtainViewModel()
        }

        viewModel = viewDataBinding.viewModel!!

        setHasOptionsMenu(true)

        viewDataBinding.setLifecycleOwner(this)

        return viewDataBinding.root
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.menu_filter -> showFilteringPopUpMenu()
        }
        return true
    }

    private fun showFilteringPopUpMenu() {
        val popup = PopupMenu(context!!, activity!!.findViewById(R.id.menu_filter))
        popup.menuInflater.inflate(R.menu.adlist_fragment_2nd_menu, popup.menu)

        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.all -> viewModel.setFiltering(AdFilterType.ALL_FILTER)
                R.id.active -> viewModel.setFiltering(AdFilterType.FAVORITE_FILTER)
                R.id.gotoClassfiedAdList -> startActivity(Intent(activity, MainActivity::class.java))
            }

            viewModel.loadAds()
            true
        }

        popup.show()
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.adlist_fragment_menu, menu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adlist_recycler_view.adapter = AdListAdapter(viewModel)
        adlist_recycler_view.addItemDecoration(GridDivider(resources.getDimension(R.dimen.divider_adlist).toInt()))
        adlist_swipe_to_refresh.setOnRefreshListener {
            viewModel.loadAds()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.loadAds()
    }
}
