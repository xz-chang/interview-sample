package org.cxz.finninterview.data.source.local

import android.arch.persistence.room.*
import io.reactivex.Single
import org.cxz.finninterview.data.Advertisement


@Dao
interface AdDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAds(users: List<Advertisement>)

    @Update
    fun updateAds(vararg users: Advertisement)

    @Query("SELECT * FROM advertisement")
    fun loadAllAds(): Single<List<Advertisement>>

    @Query( "SELECT * FROM advertisement LIMIT :limit OFFSET :start")
    fun loadAds(start: Int, limit:Int): Single<List<Advertisement>>

    @Query("DELETE FROM advertisement")
    fun deleteAllAds()

}