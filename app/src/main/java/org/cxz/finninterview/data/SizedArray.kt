package org.cxz.finninterview.data

data class SizedArray<T>(val size: Int, val items: List<T>) {
}