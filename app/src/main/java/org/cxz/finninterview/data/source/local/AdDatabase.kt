package org.cxz.finninterview.data.source.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import org.cxz.finninterview.data.AdFavorite
import org.cxz.finninterview.data.Advertisement

@Database(entities = [Advertisement::class, AdFavorite::class], version = 1)
abstract class AdDatabase: RoomDatabase() {
    abstract fun adDatabaseDAO(): AdDao
    abstract fun adFavDatabaseDAO(): AdFavDao

    companion object {
        private var INSTANCE: AdDatabase? = null

        fun getInstance(context: Context): AdDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.applicationContext, AdDatabase::class.java, "advertisement.db").build()
            }

            return INSTANCE!!
        }
    }
}