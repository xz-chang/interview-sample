/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.cxz.finninterview.data.source

import io.reactivex.Observable
import org.cxz.finninterview.data.Advertisement

interface AdsDataSource {

    interface FavoriteSavedCallback {
        fun onAdFavSaved()
    }

    interface LoadAdFavsCallback {
        fun onAdFavsLoaded(favAdIds: HashSet<Long>)
    }

    interface LoadAdsCallback {

        fun onAdsLoaded(ads: List<Advertisement>)

        fun onDataNotAvailable()
    }


    fun getAds(): Observable<List<Advertisement>>

    fun deleteAllAds()
}
