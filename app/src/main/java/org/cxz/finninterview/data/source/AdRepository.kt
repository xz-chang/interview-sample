package org.cxz.finninterview.data.source

import android.app.Application
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import org.cxz.finninterview.data.Advertisement
import org.cxz.finninterview.data.source.local.AdLocalDataSource
import org.cxz.finninterview.data.source.remote.AdRemoteDataSource
import java.util.*


class AdRepository(val app: Application): AdsDataSource {
    private val local = AdLocalDataSource(app)
    private val remote = AdRemoteDataSource(app)

    override fun getAds(): Observable<List<Advertisement>> {
        val localAds = local.getAds()
        val remoteAds = remote.getAds()

        val adFavOb: Observable<List<Long>> = local.getAdFav().toObservable()

        val localAdWithFav: Single<List<Advertisement>> = localAds.flatMapIterable { it }.withLatestFrom(adFavOb, BiFunction<Advertisement, List<Long>, Advertisement> { t1, t2 ->
            t1.isFavorite = t2.contains(t1.id)
            t1
        }).toList()

        val remoteAdWithFav: Single<List<Advertisement>> = remoteAds
                .map {
                    adList -> local.saveAds(adList)
                    adList }
                .flatMapIterable { it }
                .withLatestFrom( adFavOb, BiFunction<Advertisement, List<Long>, Advertisement> {
                    t1, t2 ->
                    t1.isFavorite = t2.contains(t1.id)
                    t1
                }).toList()

        return Observable
                .concat(localAdWithFav.toObservable(), remoteAdWithFav.toObservable())
                .distinct()
                .filter { it.isNotEmpty() }
    }

    fun setFavorite(adId: Long, isFav: Boolean, callback: AdsDataSource.FavoriteSavedCallback) {
        local.setFavorite(adId, isFav, callback)
    }

    override fun deleteAllAds() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    
    fun getLocalAds(): Observable<List<Advertisement>> {
        return local.getAds()
    }

    fun getLocalRandomAds(): Observable<List<Advertisement>> {
        val random = Random()
        val start = random.nextInt(500);
        return local.getAds(start, 20)
    }

}
