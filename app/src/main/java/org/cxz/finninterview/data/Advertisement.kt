package org.cxz.finninterview.data

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

// We mark this isFavorite as Ignore for performance concern.
@Entity
data class Advertisement(@PrimaryKey var id: Long, @Embedded var image: Image?, var description: String?, var location: String?, @Ignore var isFavorite: Boolean?) {
    constructor(): this(0, null, null, null, null)
}