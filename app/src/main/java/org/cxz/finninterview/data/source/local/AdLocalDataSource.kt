package org.cxz.finninterview.data.source.local

import android.app.Application
import android.util.Log
import io.reactivex.Observable
import io.reactivex.Single
import org.cxz.finninterview.data.AdFavorite
import org.cxz.finninterview.data.Advertisement
import org.cxz.finninterview.data.source.AdsDataSource
import org.cxz.finninterview.util.AppExecutors

class AdLocalDataSource(app: Application): AdsDataSource {
    private val adDao: AdDao = AdDatabase.getInstance(app.applicationContext).adDatabaseDAO()
    private val adFavDao: AdFavDao = AdDatabase.getInstance(app.applicationContext).adFavDatabaseDAO()
    private val appExecutor = AppExecutors()

    override fun getAds(): Observable<List<Advertisement>> {
        Log.v(AdLocalDataSource::class.java.simpleName, "fetched from local")
        return adDao.loadAllAds().toObservable()
    }

    fun getAds(start: Int, limit: Int): Observable<List<Advertisement>> {
        return adDao.loadAds(start, limit).toObservable()
    }

    fun setFavorite(adId: Long, isFav: Boolean,  callback: AdsDataSource.FavoriteSavedCallback) {
        appExecutor.diskIO().execute {
            if (isFav) {
                adFavDao.insertAdFav(AdFavorite(adId, false))
            } else {
                adFavDao.deleteAdFav(AdFavorite(adId, false))
            }

            appExecutor.mainThread().execute {
                callback.onAdFavSaved()
            }
        }
    }

    override fun deleteAllAds() {
        appExecutor.diskIO().execute {
            adDao.deleteAllAds()
        }
    }

    fun saveAds(ads: List<Advertisement>) {
        Log.v(AdLocalDataSource::class.java.simpleName, "Persist to DB")
        appExecutor.diskIO().execute {
            adDao.insertAds(ads)
        }
    }

    fun getAdFav(): Single<List<Long>> {
        return adFavDao.loadAllAdFav().toObservable().flatMapIterable { it }.map { it.advertisementId }.toList()
    }
}