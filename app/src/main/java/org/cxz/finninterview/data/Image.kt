package org.cxz.finninterview.data

class Image(val scalable:Boolean?, val width:Int?, val height:Int?, val type: String?, val url: String?) {
}