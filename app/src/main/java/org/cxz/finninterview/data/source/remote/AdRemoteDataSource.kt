package org.cxz.finninterview.data.source.remote

import android.app.Application
import io.reactivex.Observable
import org.cxz.finninterview.common.network.API
import org.cxz.finninterview.common.network.NetworkModule
import org.cxz.finninterview.data.Advertisement
import org.cxz.finninterview.data.source.AdsDataSource

class AdRemoteDataSource(val app: Application): AdsDataSource {
    override fun getAds(): Observable<List<Advertisement>> {
        return api.getAdList().map { it.items }.map { it.sortedWith(compareBy { it.id }) }.toObservable()
    }

    override fun deleteAllAds() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    val api: API = NetworkModule().provideRetrofit(app).create(API::class.java)

}