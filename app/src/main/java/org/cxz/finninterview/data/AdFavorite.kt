package org.cxz.finninterview.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class AdFavorite(@PrimaryKey var advertisementId: Long, var synced: Boolean) {
}