package org.cxz.finninterview.data.source.local

import android.arch.persistence.room.*
import io.reactivex.Single
import org.cxz.finninterview.data.AdFavorite

@Dao
interface AdFavDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAdFav(adFav: AdFavorite)

    @Delete
    fun deleteAdFav(adFav: AdFavorite)

    @Query("SELECT * from AdFavorite")
    fun loadAllAdFav(): Single<List<AdFavorite>>

    @Query("SELECT * from AdFavorite")
    fun loadAllAdFavSync(): List<AdFavorite>
}