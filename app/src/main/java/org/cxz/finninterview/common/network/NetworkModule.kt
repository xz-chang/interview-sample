package org.cxz.finninterview.common.network

import android.content.Context
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executor
import java.util.concurrent.Executors

private const val BASE_URL = "https://gist.githubusercontent.com/changxiangzhong/c86f74407a1250788c1ecd1ca7a3caa0/raw/6d5bd50234de7b868f1c9a1e52e658deb40d160f/"

@Module
class NetworkModule {

    @Provides
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return gsonBuilder.create()
    }

    @Provides
    fun provideCache(context: Context): Cache {
        val cacheSize = 10 * 1024 * 1024 // 10 MB
        return Cache(context.externalCacheDir!!, cacheSize.toLong())
    }

    @Provides
    fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory {
        return GsonConverterFactory.create(gson)
    }

    @Provides
    fun provideRetrofit(executor: Executor, okHttpClient: OkHttpClient, gsonConverterFactory: GsonConverterFactory): Retrofit {
        return Retrofit.Builder().addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .callbackExecutor(executor)
                .client(okHttpClient)
                .build()
    }

    @Provides
    fun provideSingleThreadExecutor(): Executor {
        return Executors.newSingleThreadExecutor()
    }

    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val builder = OkHttpClient.Builder()
        builder.addInterceptor(logInterceptor)
        return builder.build()
    }

    fun provideRetrofit(context: Context): Retrofit {
        return provideRetrofit(
                provideSingleThreadExecutor(),
                provideOkHttpClient(),
                provideGsonConverterFactory(provideGson())
        )
    }
}