package org.cxz.finninterview.common.network

import io.reactivex.Single
import org.cxz.finninterview.data.Advertisement
import org.cxz.finninterview.data.SizedArray
import retrofit2.http.GET

interface API {
    @GET("interview-sample.json")
    fun getAdList(): Single<SizedArray<Advertisement>>
}