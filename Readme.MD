# Update 3rd July
At the beginning, I was thinking the SizedArray was pretty 'old fashion' - in C programming you have to know the size before
you can allocate memory. Such practice is not popular anymore in Java world.

But after a second thinking, especially after I have seen the size of payload - it's more than 1.6MB. A rough estimation of memory consumption
 is 6k * sizeof (Advertisement) = 6k * (8 + 20 * 1 + 30 * 1 + 4 + 40 * 1) = 6k * 100B = 600KB. If we are going to add more properties in the
data model `Advertisement`. This could easily reach 6K * 1.6KB = 9.6MB.

With `TypeAdapter` in gson, we can definitely improve the memory usage. If we can known the size in advance, we can definitely
optimize the memory consumption and reduce the GC times. Because for both `array` and `linked-list` when large amount of items
are inserted, it would consume considerable amount of time.

# Description of the solution

###The whole working flow for `Repository layer` is
 - The `ViewModel` initiate a request with a `Callback` function as the parameter
 - The `Repository` will try to query the local storage, the `Callback` will be called if there is any local storage
 - After that, the `Repository` will query the remote server, the `Callback` will be trigger once again. 
 - And the local cache stored in local storage will be refresh whatsoever.

If we could apply etags, we might be able to spare the 2nd callback in case of *no-content-change* and save some bandwidth
 
 
###Concerning the persistence layer, we get two table/entities.
 - `Advertisement` - this is corresponding to the json payload from the server
 - `AdvertisementFavorite` - this is corresponding to the favorite state.

 The benfit of having 2 tables is 1) we can simply clear the `Advertisement` table without caring about the he 
 `favorite states`.
 
 There might be "orphan data" in `AdvertisementFavorite` table, but considering if size, it should be ok

###In this project we have applied the following things
 - MVVM architecture
 - Android architecture component
 - ROOM persistence layer
 - Retrofit
 - Databinding
 
 
# Good things
 - It works, haven't find any major issue.
 - Android Databinding is awesome, this is my first time to try my best to reduce the binding code and it's really clean
 - We have chosen ROOM, because it support the `@Embedded` association, which means, it will not create another table for
 the `Image` model. There's no primary key in `Image` model, generating a key at client side will be very annoying, let
 alone *cascade remove*, *cascade update* and etcs.

# Things that can be improved
 0) I've underestimate the workload of this project, I could have try more stuffs such as `RxJava` and `Dagger`
 
 1) *Callback hell* there are 3 kinds of callbacks are mixed together, I'm not happy about it. RxJava can be the cure for 
 those nested callbacks
    - Callback for `LocalDataSource`
    - Callback for `RemoteDataSource`
    - Callback for `LocalFavoriteStates`
    
 2) In `AdRepository` there are quite a few code about dispatch certain operations to `worker-thread` and `main-thread`.
Those dispatch things are quite ugly as well. Again RxJava can be the cure with `subscribeOn` and `observeOn`

 3) `Dagger` can be used for optimizing the loading logic of the project - but this is a fairly small project, and there
 is not clear `scopes` within the lifecycle of the App (There's only one scope - `AdvertisementList`)
 
 4) The whole gson payload is fairly huge - 1.6MB. I should definitely try etag to reduce the bandwidth consumption.
 
 5) Testing should be conducted.
  